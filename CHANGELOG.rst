0.9a2 (unreleased)
------------------

- Nothing changed yet.


0.9a1 (2019-04-04)
------------------

- adapt version handling
- improve error handling


0.9a0 (2019-04-02)
------------------

- add form post handler
- validate form content
