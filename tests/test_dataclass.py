from datetime import date
from datetime import timedelta
from eloquaformhandler import dataclass
from faker import Faker
from unittest.mock import Mock

import pytest


today = date.today()
last_week = date.today() - timedelta(days=7)
faker = Faker()


def test_empty_field_converter():
    converter = dataclass.FieldConverter(None, {})
    assert {'fieldValues': []} == converter({})


def test_field_converter():
    form_id = faker.random_int()
    html_name = faker.slug()
    id_name = faker.slug()
    value_name = faker.name()
    converter = dataclass.FieldConverter.from_schema({
        'id': form_id,
        'elements': [{'htmlName': html_name, 'id': id_name}],
    })
    assert {
        'fieldValues': [{
            'id': id_name, 'type': 'FieldValue', 'value': value_name
        }]
    } == converter({html_name: value_name})


def test_field_converter_unknown_input():
    form_id = faker.random_int()
    html_name = faker.slug()
    id_name = faker.slug()
    unknown_id = faker.slug()
    unknown_value = faker.slug()
    converter = dataclass.FieldConverter.from_schema({
        'id': form_id,
        'elements': [{'htmlName': html_name, 'id': id_name}],
    })
    pytest.raises(KeyError, converter, {unknown_id: unknown_value})


def test_field_converter_missing_input():
    html_name = faker.slug()
    id_name = faker.slug()
    converter = dataclass.FieldConverter.from_schema({
        'id': 0,
        'elements': [{'htmlName': html_name, 'id': id_name}],
    })
    assert {'fieldValues': []} == converter({})


def test_empty_schema():
    old_schema = faker.slug()
    session = Mock()
    schema = dataclass.Schema(
        form_id=faker.random_number(),
        last_retrieved=today,
        base_url=faker.url(),
        session=session
    )
    schema._cached_schema = old_schema
    session.get.assert_not_called()
    assert old_schema == schema()


def test_old_schema():
    old_schema = faker.slug()
    session = Mock()
    schema = dataclass.Schema(
        form_id=faker.random_number(),
        last_retrieved=last_week,
        base_url=faker.url(),
        session=session
    )
    schema._cached_schema = old_schema
    assert old_schema != schema()
    session.get.assert_called()


def test_schema_is_old():
    old_schema = faker.slug()
    session = Mock()
    schema = dataclass.Schema.from_id(
        form_id=faker.random_number(),
        base_url=faker.url(),
        session=session,
    )
    schema._cached_schema = old_schema
    assert old_schema != schema()
    session.get.assert_called()


def test_schema_returns_json():
    session = Mock()
    dataclass.Schema.from_id(
        form_id=faker.random_number(),
        base_url=faker.url(),
        session=session,
    )()
    session.get.assert_called()
    session.get().json.assert_called()


def test_schema_checks_for_errors():
    session = Mock()
    dataclass.Schema.from_id(
        form_id=faker.random_number(),
        base_url=faker.url(),
        session=session,
    )()
    session.get.assert_called()
    session.get().raise_for_status.assert_called()


def test_transmit_empty():
    session = Mock()
    session.post().status_code = 201
    transmit = dataclass.Transmit(
        form_id=faker.random_number(),
        base_url=faker.url(),
        session=session,
    )
    assert session.post().status_code == transmit(form_data=None)


def test_transmit_handle_200_as_failure():
    session = Mock()
    session.post().status_code = 200
    transmit = dataclass.Transmit(
        form_id=faker.random_number(),
        base_url=faker.url(),
        session=session,
    )
    pytest.raises(AssertionError, transmit, None)


def test_validate_data_is_sent_and_url_is_valid():
    session = Mock()
    session.post().status_code = 201
    form_id = faker.random_number()
    base_url = faker.url()
    fake_data = faker.slug()
    transmit = dataclass.Transmit(
        form_id=form_id,
        base_url=base_url,
        session=session,
    )
    transmit(form_data=fake_data)
    session.post.assert_called_with(
        '{base_url}/api/REST/2.0/data/form/{form_id}'.format(
            base_url=base_url, form_id=form_id
        ),
        json=fake_data
    )
