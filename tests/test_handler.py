from eloquaformhandler.exceptions import EloquaError
from eloquaformhandler.handler import HandlerFactory
from httmock import all_requests, HTTMock
import json
from pathlib import Path
import requests
import pytest

TEST_FOLDER = Path(__file__).parent
DATA_FOLDER = TEST_FOLDER / 'data'


@all_requests
def response_not_authorized(url, request):
    return {'status_code': 200, 'content':  '"Not authenticated."'}


@all_requests
def response_empty_json(url, request):
    return {'status_code': 200, 'content': {}}


@all_requests
def response_empty(url, request):
    return {'status_code': 200, 'content': ''}


@all_requests
def response_no_content(url, request):
    return {'status_code': 204, 'content': None}


@all_requests
def response_status_500(url, request):
    return {'status_code': 500, 'content': 'Error 500'}


@all_requests
def response_status_404(url, request):
    return {'status_code': 404, 'content': 'Error 404'}


@all_requests
def response_login_json(url, request):
    with open(DATA_FOLDER/'login_response.json') as f:
        json_response = json.load(f)
    return {'status_code': 200, 'content': json_response}


def test_unauthorized():
    session = requests.Session()

    with HTTMock(response_not_authorized), \
         pytest.raises(EloquaError, match='Not authenticated.'):  # NOQA
        HandlerFactory.get(session)(1)


def test_unexpected_json():
    session = requests.Session()

    with HTTMock(response_empty_json), pytest.raises(KeyError):
        HandlerFactory.get(session)(1)


def test_unexpected_content():
    session = requests.Session()

    with HTTMock(response_empty), pytest.raises(json.JSONDecodeError):
        HandlerFactory.get(session)(1)


def test_no_content():
    session = requests.Session()

    with HTTMock(response_no_content), \
         pytest.raises(ValueError):  # NOQA
        HandlerFactory.get(session)(1)


def test_status_500():
    session = requests.Session()

    with HTTMock(response_status_500), \
         pytest.raises(requests.HTTPError):  # NOQA
        HandlerFactory.get(session)(1)


def test_status_404():
    session = requests.Session()

    with HTTMock(response_status_404), \
         pytest.raises(requests.HTTPError):  # NOQA
        HandlerFactory.get(session)(1)


def test_authenticated():
    session = requests.Session()

    with HTTMock(response_login_json):
        handler_factory = HandlerFactory.get(session)
    assert handler_factory.base_url == 'https://secure.example.eloqua.com'
