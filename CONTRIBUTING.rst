.. _contributing:

Contributing
============

If you find some bugs or would suggest a new feature please open an issue.
We are open to merge requests, but ensure there exists an issue before summarizing the bug/feature.

Coding style
------------

Python coding style for eloqua-formhandler is `PEP 8`_.

.. _PEP 8: https://www.python.org/dev/peps/pep-0008

Testing
-------

Please write tests as part of your merge requests.

Documentation
-------------

If your MR adds a new feature add it to the documentation/readme.
