class MissingRequired(Exception):
    pass


class EloquaError(RuntimeError):
    pass
