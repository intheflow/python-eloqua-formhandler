.. eloquaformhandler documentation master file, created by
   sphinx-quickstart on Tue Apr  2 11:06:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to eloquaformhandler's documentation!
=============================================

.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
